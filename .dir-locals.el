;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Directory-Variables.html

;; https://gitlab.inria.fr/elementaryx/elementaryx-cicd
((nil . ((org-html-link-home . "https://elementaryx.gitlabpages.inria.fr/")
	 (org-html-link-up . "./index.html"))))

