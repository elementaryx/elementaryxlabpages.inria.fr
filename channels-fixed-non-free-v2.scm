;; Version 2.1.2 of ElementaryX with Guix-HPC-Non-Free
(list
 (channel
  (name 'guix-hpc-non-free)
  (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git")
  (branch "master")
  (commit "fd4c8c27f4567e5d1329beb0eb59b7850e437c7f"))

 (channel
  (name 'guix)
  (url "https://git.savannah.gnu.org/git/guix.git")
  (branch "master")
  (commit "2f0644a8c024f589b9237e1d0fb1ce1025fa522f")
  (introduction
   (make-channel-introduction
    "9edb3f66fd807b096b48283debdcddccfea34bad"
    (openpgp-fingerprint
     "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))

 (channel
  (name 'guix-hpc)
  (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
  (branch "master")
  (commit "22f97efea70bd18fc9a6ca93c19e36771bd9f873"))

 (channel
  (name 'guix-science-nonfree)
  (url "https://github.com/guix-science/guix-science-nonfree.git")
  (branch "master")
  (commit "3413215081da370823c98e79445d8cbfd37531db")
  (introduction
   (make-channel-introduction
    "58661b110325fd5d9b40e6f0177cc486a615817e"
    (openpgp-fingerprint
     "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))

 (channel
  (name 'guix-science)
  (url "https://github.com/guix-science/guix-science.git")
  (branch "master")
  (commit "3736b80cf569511cc0a13f87cb03411761290dd8")
  (introduction
   (make-channel-introduction
    "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
    (openpgp-fingerprint
     "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))

 (channel
  (name 'guix-past)
  (url "https://gitlab.inria.fr/guix-hpc/guix-past")
  (branch "master")
  (commit "792d830606345bddf5712252a1ed4fd87ee55c42")
  (introduction
   (make-channel-introduction
    "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
    (openpgp-fingerprint
     "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5")))))
