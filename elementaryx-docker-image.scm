;; -*- mode: scheme; -*-

;; This operating system aims at providing elementaryx with guix and
;; guix-hpc support. It is inspired from the bare-bones.templ and
;; docker-image.templ from configuration template for a "Docker image"
;; from guix git repository (gnu/system/examples repository).

(use-modules (gnu) (guix channels))
(use-service-modules networking ssh)
(use-package-modules screen certs package-management ssh) ;; package-management for current-guix

;; (define %elementaryx-channels
;;   (list (channel
;;          (name 'guix)
;;          (url "https://git.savannah.gnu.org/git/guix.git")
;;          (branch "master")
;;          (commit
;; 	  "40f1f5b0ca6bfca8e069a6453306699803f8c75d")
;;          (introduction
;; 	  (make-channel-introduction
;; 	   "9edb3f66fd807b096b48283debdcddccfea34bad"
;; 	   (openpgp-fingerprint
;; 	    "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
;; 	(channel
;;          (name 'guix-hpc)
;;          (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
;;          (branch "master")
;;          (commit
;; 	  "021d076e05baf63ea8fa5671e469a856bc2dd462"))))

(operating-system
  (host-name "village")
  (timezone "Europe/Paris")
  (locale "en_US.utf8")

  ;; This is where user accounts are specified.  The "root" account is
  ;; implicit, and is initially created with the empty password.
  (users (cons (user-account
                (name "panoramix")
                (comment "the guix druid who sees everything aka getafix")
                (group "users")
                (supplementary-groups '("wheel"
                                        "audio" "video")))
               %base-user-accounts))

  ;; Globally-installed packages.
  (packages
   (append
    (list (specification->package "emacs-elementaryx-as-default")
          (specification->package "icecat")
          (specification->package "nss-certs")
          (specification->package "openssh")
          (specification->package "screen")
	  )
    %base-packages))

  ;; Because the system will run in a Docker container, we may omit many
  ;; things that would normally be required in an operating system
  ;; configuration file.  These things include:
  ;;
  ;;   * bootloader
  ;;   * file-systems
  ;;   * services such as mingetty, udevd, slim, networking, dhcp
  ;;
  ;; Either these things are simply not required, or Docker provides
  ;; similar services for us.

 ;; This will be ignored.
 (bootloader (bootloader-configuration
              (bootloader grub-bootloader)
              (targets '("does-not-matter"))))
  ;; This will be ignored, too.
  (file-systems (list (file-system
                        (device "does-not-matter")
                        (mount-point "/")
                        (type "does-not-matter"))))

  ;; As in the bare-bones template, we add services a DHCP client and
  ;; an SSH server. Note that one may wish to add an NTP service here
  ;; as well. Contrary to the bare-bones template which adds these
  ;; services to the %base-services, we simply add these services to
  ;; the guix-service-type. Indeed the guix-service-type service would
  ;; be enough for a docker image (it is the only service used in the
  ;; docker-image.tmpl). However, contrary to the docker-image.tmpl,
  ;; the guix-service-type we set up is enhanced with:
  ;; - current-guix
  ;; - support for additional substitutes.
  ;; (services
  (services (append (list (service dhcp-client-service-type)
                          (service openssh-service-type
                                   (openssh-configuration
                                    (openssh openssh-sans-x)
                                    (port-number 2222)))
			  (simple-service 'elementaryx-channels etc-service-type
					  `(("guix/channels.scm" ,(scheme-file "channels.scm"
									       #~(cons (channel
											(name 'guix-hpc)
											(url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git"))
											       %default-channels)))))
			  ;; (simple-service 'elementaryx-channels-service etc-service-type
			  ;; 		  `(("guix/channels.scm" ,(scheme-file "channels.scm"
			  ;; 						       #~%elementaryx-channels))))
					  )
		    ;;                    %base-services)))
		    ;; Instead of %base-services, we only use
		    ;; guix-service-type, which we modify to
		    ;; support additional substitute-urls
		    (modify-services %base-services
;;		    (modify-services (list (service guix-service-type))
		      (guix-service-type config =>
				   (guix-configuration
				    (inherit config)
				    ;; Install and run the current
				    ;; Guix rather than an older
				    ;; snapshot, as in vm-image.tmpl
				    ;; TODO read https://guix.gnu.org/manual/en/html_node/Base-Services.html#index-guix_002dservice_002dtype guix-configuration 
				    (guix (current-guix))
				    ;; Enable Additional substitute-urls
				    (substitute-urls '("https://ci.guix.gnu.org"
						       "https://guix.bordeaux.inria.fr"
						       "https://substitutes.nonguix.org"))
				    (authorized-keys
				     (append (list (local-file "./guix-hpc-key.txt")
						   (local-file "./nonguix-key.pub"))
					     %default-authorized-guix-keys))))))))


