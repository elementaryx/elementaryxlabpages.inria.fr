(require 'use-package)

(defun elementaryx-try-load-org-or-el (org-file el-file)
  "Try to load ORG-FILE if it exists; otherwise, try to load EL-FILE if it exists."
  (if (file-exists-p org-file)
      (org-babel-load-file org-file)
    (when (file-exists-p el-file)
      (load-file el-file))))

;; Define the paths to the custom initialization files
(defvar my-early-custom-el-file (expand-file-name "init-elementaryx-custom-early.el" user-emacs-directory)
  "Path to the early custom initialization file in .el format.")
(defvar my-early-custom-org-file (expand-file-name "init-elementaryx-custom-early.org" user-emacs-directory)
  "Path to the early custom initialization file in .org format.")
(defvar my-late-custom-el-file (expand-file-name "init-elementaryx-custom.el" user-emacs-directory)
  "Path to the late custom initialization file in .el format.")
(defvar my-late-custom-org-file (expand-file-name "init-elementaryx-custom.org" user-emacs-directory)
  "Path to the late custom initialization file in .org format.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Load the early custom initialization file if it exists ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(elementaryx-try-load-org-or-el my-early-custom-org-file my-early-custom-el-file)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Perform elementaryx initialization ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Define a custom message function for ElementaryX with a prefix.
(defun elementaryx-message (format-string &rest args)
  "Display a message with the prefix 'ElementaryX: '.
FORMAT-STRING is the format string to display.
ARGS are the arguments to format the message."
  (apply 'message (concat "ElementaryX: " format-string) args))

;; Define a global variable `elementaryx-editor` to hold the current editor setup.
(defcustom elementaryx-editor nil
  "Current ElementaryX editor setup.
Possible values are:
- 'emacs'
- 'emacs-full' (alias for 'emacs')
- 'vym'
- 'escode'
- 'minimal'
- 'base'
- 'ox'
- 'ox-publish' (alias for 'ox')
- 'vanilla'
- 'q' (alias for 'vanilla')

Any other value is treated as a custom setup and is at the user's discretion."
  :type '(choice
          (const :tag "Emacs" 'emacs)
          (const :tag "Emacs (alias)" 'emacs-full)
          (const :tag "Vym" 'vym)
          (const :tag "ElementaryX Studio Code" 'escode)
          (const :tag "Minimal" 'minimal)
          (const :tag "Base" 'base)
          (const :tag "Org Publish" 'ox)
          (const :tag "Org Publish (alias)" 'ox-publish)
          (const :tag "Vanilla" 'vanilla)
          (const :tag "Custom" nil))
  :group 'elementaryx)

;; ;; Define a global variable `elementaryx-editor` to hold the current editor setup.
;; (defvar elementaryx-editor nil
;;   "Current ElementaryX editor setup.
;; Possible values are:
;; - 'emacs'
;; - 'emacs-full' (alias for 'emacs')
;; - 'vym'
;; - 'escode'
;; - 'minimal'
;; - 'base'
;; - 'ox'
;; - 'ox-publish' (alias for 'ox')
;; - 'vanilla'
;; - 'q' (alias for 'vanilla')
;; Any other value is treated as a custom setup and is at the user's discretion.")

(defun elementaryx-initialize-setup ()
  "Initialize the ElementaryX setup based on the ELEMENTARYX_EDITOR environment variable.
If `elementaryx-editor` is already set, it is not changed. Otherwise, it is set according to the environment variable,
and falls back to 'emacs' if the environment variable is not set."
  ;; Check if `elementaryx-editor` is already set. If so, don't change it.
  (unless elementaryx-editor
    ;; Retrieve the value of the ELEMENTARYX_EDITOR environment variable.
    (let ((editor (getenv "ELEMENTARYX_EDITOR")))
      ;; Set the `elementaryx-editor` variable based on the environment variable or default to 'emacs'.
      (setq elementaryx-editor
            (cond
             ;; If the environment variable is not set, default to 'emacs'.
             ((not editor) "emacs")

             ;; If set to valid predefined values, use them for `elementaryx-editor`.
             ((member editor '("emacs" "emacs-full" "vym" "escode" "minimal" "base" "ox" "ox-publish" "vanilla" "q"))
              editor)

             ;; Otherwise, treat it as a custom setup, warn the user, and accept the value.
             (t (progn
                  (elementaryx-message "Warning: '%s' is a custom setup. Proceeding with caution." editor)
                  editor)))))

    ;; Map aliases to their respective setups.
    (setq elementaryx-editor
          (cond
           ;; Map "emacs-full" to "emacs".
           ((equal elementaryx-editor "emacs-full") "emacs")

	   ;; Map "ox-publish" to "ox".
           ((equal elementaryx-editor "ox-publish") "ox")

           ;; Map "q" to "vanilla".
           ((equal elementaryx-editor "q") "vanilla")

           ;; Otherwise, use the value as-is.
           (t elementaryx-editor))))

  ;; Print a message indicating which setup is being initialized.
  (elementaryx-message "Current setup: %s" elementaryx-editor)

  ;; Load the corresponding configuration based on `elementaryx-editor`.
  (cond
   ;; Initialize the Emacs or Emacs-full setup.
   ((equal elementaryx-editor "emacs")
    (elementaryx-message "Loading Emacs setup...")
    ;; Load Emacs-specific setup here.
    (use-package elementaryx-full)
    )

   ;; Initialize the Vym setup.
   ((equal elementaryx-editor "vym")
    (elementaryx-message "Loading Vym setup...")
    ;; Load Vym-specific setup here.
    (use-package elementaryx-full)
    (use-package elementaryx-evil)
    )

   ;; Initialize the Escode (VSCode-like) setup.
   ((equal elementaryx-editor "escode")
    (elementaryx-message "Loading ESCode setup...")
    ;; Load ESCode setup here.
    (use-package elementaryx-escode)
    )

   ;; Initialize the Minimal setup.
   ((equal elementaryx-editor "minimal")
    (elementaryx-message "Loading Minimal setup...")
    ;; Load minimal setup here.
    (use-package elementaryx-minimal)
    )

   ;; Initialize the Base setup.
   ((equal elementaryx-editor "base")
    (elementaryx-message "Loading Base setup...")
    ;; Load base setup here.
    (use-package elementaryx-base)
    )

   ;; Initialize the Ox setup.
   ((equal elementaryx-editor "ox")
    (elementaryx-message "Loading Ox setup...")
    ;; Load Ox-Publish setup here.
    (use-package elementaryx-ox-publish)
    )

   ;; Initialize the Vanilla or Q setup.
   ((equal elementaryx-editor "vanilla")
    (elementaryx-message "Loading Vanilla setup...")
    ;; Load vanilla (or q) setup here.
    ;; => Nothing to do.
    )

   ;; Handle fully custom named setup.
   (t
    (elementaryx-message "Warning: Custom setup '%s' is being used. Proceeding without predefined configurations." elementaryx-editor)
    ;; This is for handling fully custom named setup, left open-ended for the user to entirely handle it in its custom setup.
    )))

;; Call the function during Emacs initialization to ensure the correct setup is applied.
(elementaryx-initialize-setup)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Load the late custom initialization file if it exists ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(elementaryx-try-load-org-or-el my-late-custom-org-file my-late-custom-el-file)

