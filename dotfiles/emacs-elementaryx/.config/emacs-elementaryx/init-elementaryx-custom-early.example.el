;; Example Early Customization File
;;
;; This file serves as a template for customizing your Emacs Elementaryx setup. It is not used by default.
;; To activate and use these customizations, follow these steps:
;;
;; 1. Rename this file to `init-elementaryx-custom-early.el'.
;; 2. Uncomment the sections you want to enable or modify.

;; The early customization shall be typically used to change the
;; loading of Emacs Elementaryx

;; --- Warnings ---

;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Warning-Options.html

;; In vanilla emacs, :warning-level (and above, i.e. :error- and
;; :emergency-level) warnings pop un the warning window. We follow
;; this trend in ElementaryX (and Vym). On the contrary VS Code does
;; not pop up warning-like warnings. We can do the same with emacs by
;; deciding which type of warnings shall pop up. For instance, in ES
;; code, only :error-level warnings (and higher, i.e.
;; :emergency-level) ones do pop up. Doing the same in baseline
;; ElementaryX (or Vym) can be achieved by uncommenting the following line:

; (setq warning-minimum-level :error)

;; Note that there exists a more fine grain mechanism to suppress
;; certain types of warnings, using `warning-suppress-types' variable.
;; For instance, one may suppress face-related warnings by
;; uncommenting the following line (you may need to adjust types based
;; on actual warnings):

; (setq warning-suppress-types '((face)))

;; --- Org-Mode ---

;; By default `elementaryx-org-directory' is defined as "~/org-roam/"
;; and is used to setup `org-directory', `org-agenda-files' and
;; `org-roam-directory' variables by `elementaryx-org' package. This
;; can be overriden by setting `elementaryx-org-directory' BEFORE
;; loading `elementaryx-org' package. Uncomment and tune the following
;; line to do so.

; `(setq elementaryx-org-directory "~/org")'

;; It is also possible to override `org-directory', `org-agenda-files'
;; and `org-roam-directory' separately. In this case, it must happen
;; AFTER loading `elementaryx-org' (see
;; `./init-elementaryx-custom.example.el').

;; --- Advanced lisp scheme users ---

;; The paredit package is a widely used and highly recommended package
;; for editing Lisp and Scheme in Emacs. It helps maintain the
;; structural integrity of the code by automatically managing
;; parentheses and other delimiters. This can be particularly useful
;; in Lisp-like languages, where parentheses are crucial to the
;; syntax.

;; However, paredit can feel restrictive or unintuitive at first
;; because it prevents you from accidentally deleting parentheses that
;; would break the structure of the code. This may be frustrating
;; until you get used to the editing model. In addition, it has a
;; steep learning curve: It requires learning specific keybindings and
;; behaviors, which can initially slow down your workflow if you're
;; not accustomed to it.

;; This is why it is not enabled by default. Confirmed lisp / scheme
;; users will certainly want to auto enable it, meaning that it gets
;; automatically enabled for lisp / scheme edition. To do so,
;; uncomment the following line.

; (setq elementaryx-auto-enable-paredit-for-lisp-and-scheme t)


