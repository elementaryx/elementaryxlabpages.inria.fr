;; Example Customization File
;;
;; This file serves as a template for customizing your Emacs setup. It is not used by default.
;; To activate and use these customizations, follow these steps:
;;
;; 1. Rename this file to `init-elementaryx-custom.el'.
;; 2. Uncomment the sections you want to enable or modify.


;; --- Basic Customizations ---

;; Disable the startup message
; (setq inhibit-startup-message t)

;; Disable the menu bar
; (menu-bar-mode -1)

;; Enable the tool bar
; (tool-bar-mode 1)

;; Disable the scroll bar
; (scroll-bar-mode -1)

;; Disable mouse emulation in terminal mode
; (xterm-mouse-mode 0)

;; --- Appearance and Themes ---

;; Load a custom theme
; (load-theme 'doom-one t)  ;; Replace 'doom-one' with your preferred theme

;; Set the default font size
; (set-face-attribute 'default nil :height 120)  ;; Adjust the value as needed

;; --- Editing and Behavior ---

;; Disable auto-revert mode to automatically refresh buffers
; (global-auto-revert-mode nil)

;; Disable showing matching parentheses
; (show-paren-mode nil)

;; Disable syntax highlighting
; (global-font-lock-mode nil)

;; Configure tab width and indentation
; (setq-default tab-width 4)
; (setq-default indent-tabs-mode nil)  ;; Use spaces instead of tabs

;; --- Org Mode Customizations ---

;; Set the default directory for Org files
; (setq org-directory "~/org/")

;; Enable line wrapping in Org mode
; (add-hook 'org-mode-hook 'visual-line-mode)

;; --- Project Customizations ---

;; In Elementaryx, both `project.el' and `magit-repos' recursively
;; look for projects using the `elementaryx-project-roots' list of
;; directories (paths). It is set to '("~/project/") by default.
;; Uncomment (and tune) the following line to change it:
; (setq elementaryx-project-roots '("~/repos"))

;; Once setup, the project list can be updated through `M-x
;; elementaryx-update-project-roots' or `C-x p U'. The command may
;; take a few minutes.

;; This `elementaryx-project-roots' list is also used to set up the
;; roots of `magit-repository-directories' used by
;; `magit-list-repositories' to recursively search for projects. The
;; level of recursion must be provided. In ElementaryX it is
;; controlled through `elementaryx-magit-repository-depth' whose
;; default value is 6. Uncomment and tune the following line if you
;; want to change it:
; (setq elementaryx-magit-repository-depth 8)

;; It is also possible to setup `magit-repository-directories' at a
;; finer grain. Uncomment and tune the folloinwg line to do so:
; (setq magit-repository-directories '(("~/project/" . 6) ("~/repos/" . 8)))

;; --- Key Binding Customizations ---

;; Bind a function to a key
;  (global-set-key (kbd "C-c w") 'delete-trailing-whitespace)

;; Elementaryx reserves `C-z' for Windows-like "undo" command.
;; The `suspend-frame' command is available through `C-x C-z'.
;; To restore vanilla emacs `C-z' bound to `suspend-frame', uncomment the following line:
;  (global-set-key (kbd "C-z") 'suspend-frame)

;; Define an interactive custom function
;(defun my-custom-function ()
;  "A simple custom function that is interactive."
;  (interactive)  ;; This makes the function callable as a command
;  (message "Hello, Emacs!"))

;; Bind the interactive custom function to a key
; (global-set-key (kbd "C-c h h") 'my-custom-function)
