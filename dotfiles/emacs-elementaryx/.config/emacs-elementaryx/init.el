;; Define the relative paths to the initialization files
(defvar my-default-file (expand-file-name "init-elementaryx-default.el" user-emacs-directory)
  "Path to the default initialization file.")
(defvar my-override-file (expand-file-name "init-override.el" user-emacs-directory)
  "Path to the override initialization file.")

;; Check if the override file exists
(if (file-exists-p my-override-file)
    ;; If the override file exists, loads it (instead of the default elementaryx initialization file)
    (load-file my-override-file)
  ;; Otherwise, loads the default elementaryx initizliazation file
  (load-file my-default-file))
