;; Example Overriding Initialization File
;;
;; This file serves as a template for OVERRIDING Emacs Elementaryx
;; default loading. It is not used by default.
;;
;; WARNING. Use with caution. If you simply look for customizing Emacs
;; Elementaryx, please instead consider to use
;; `init-elementaryx-custom-early.el' and `init-elementary-custom.el'.
;; See `init.el' and `init-elementaryx-default.el' for more details.

;; If you know what you are doing and DO want to OVERRIDE Emacs
;; Elementaryx loading:
;; 1. rename this file to `init-override.el'
;; 2. customize it to your needs.

;; --- Require use-package ---

(require 'use-package)

;; --- Example similar to a customization ---

;; The following example achieve the same behaviour as:
;;
;; - setting `(setq  elementaryx-auto-enable-paredit-for-lisp-and-scheme t)' in `init-elementaryx-custom-early.el'
;;
;; - setting `(menu-bar-mode -1)' and `(global-set-key (kbd "C-z")
;; - 'suspend-frame)' in `init-elementaryx-custom.el'
;;
;; Uncomment the six following lines to do so:

; (use-package elementaryx-full
;   :init
;   (setq elementaryx-auto-enable-paredit-for-lisp-and-scheme t)
;   :config
;   (menu-bar-mode -1)
;   (global-set-key (kbd "C-z") 'suspend-frame))

;; --- Example with selective loading of elementaryx packages ---

;; In this example, we selectively load elementaryx packages, not
;; loading `elementaryx-treemacs', nor `elementaryx-all-the-icons',
;; which `elementaryx-full' would load.

;; Uncomment the six following lines to do so:

; (use-package elementaryx-early-init)
; (use-package elementaryx-org)
; (use-package elementaryx-ox-publish)
; (use-package elementaryx-dev)
; (use-package elementaryx-dev-parentheses)
; (use-package elementaryx-write)

;; --- Example with a customized loading of an elementaryx package ---

;; In this example, we customize the loading of `elementaryx-org'.
;; Uncomment the eleven following lines to do so:

; (use-package elementaryx-early-init)
; (use-package elementaryx-org
;   :init
;   (setq elementaryx-org-directory "~/org"))
; (use-package elementaryx-org)
; (use-package elementaryx-ox-publish)
; (use-package elementaryx-dev)
; (use-package elementaryx-dev-parentheses)
; (use-package elementaryx-write)
; (use-package elementaryx-treemacs)
; (use-package elementaryx-all-the-icons)

;; Note that a similar behaviour may be achieved more simply using
;; `./init-elementaryx-custom-early.el' (see
;; `./init-elementaryx-custom-early.example.el').

;; Note that it is also possible to more synthetically apply it to
;; `elementaryx-full' by uncommenting the three following lines:

; (use-package elementaryx-full
;   :init
;   (setq elementaryx-org-directory "~/org"))

