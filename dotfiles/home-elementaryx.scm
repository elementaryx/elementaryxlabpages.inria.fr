(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp)
	     (gnu home services dotfiles)
             (gnu home services shells))

(home-environment
 (packages (specifications->packages (list "emacs-elementaryx"
					   "elementaryx-vym"
					   "elementaryx-escode"
					   "glibc"
					   "glibc-locales"
                                           "icecat"
					   "vim"
					   "xset")))
 (services
  (list  (service home-dotfiles-service-type
		  (home-dotfiles-configuration
		   (directories (list "bash" "bash-elementaryx" "emacs-elementaryx" "gnureadline" )))))))


