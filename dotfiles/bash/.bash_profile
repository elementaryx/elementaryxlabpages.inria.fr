# -*- mode: sh -*-

#####################################################################
# Example of a .bash_profile coupled with .bash_profile.elementaryx #
#####################################################################

# The bash manual states: "When bash is invoked as an interactive
# login shell, or as a non-interactive shell with the --login option,
# it first reads and executes commands from the file /etc/profile, if
# that file exists. After reading that file, it looks for
# ~/.bash_profile, ~/.bash_login, and ~/.profile, in that order, and
# reads and executes commands from the first one that exists and is
# readable."

# Therefore, if the present file is setup as your ~/.bash_profile,
# bash invoked as a login shell will load it after your /etc/profile
# system file (if that one exists too).

###################
# ############### #
# # Elementaryx # #
# ############### #
###################

# Load "elementaryx .bashr_profile", if available
if [ -f ~/.bash_profile.elementaryx ]; then . ~/.bash_profile.elementaryx; fi

##################
# ############## #
# Honor .profile #
# ############## #
##################

# It is more robust to rely on ~/.profile. In particular, it is
# required by guix home, which generates and handles ~/.profile, see
# https://guix.gnu.org/manual/devel/en/html_node/Configuring-the-Shell.html
if [ -f ~/.profile ]; then . ~/.profile; fi

#################
# ############# #
# Personal PATH #
# ############# #
#################

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

###########################
# ####################### #
# GNU Readline (.inputrc) #
# ####################### #
###########################

# Check if /etc/inputrc, ~/.inputrc_with_system, and ~/.inputrc all exist
if [ -f /etc/inputrc ] && [ -f ~/.inputrc_with_system ] && [ -f ~/.inputrc ]; then
    # If they all exist, use ~/.inputrc_with_system as the INPUTRC
    # file. This file will load /etc/inputrc and (then) ~/.inputrc
    export INPUTRC=~/.inputrc_with_system
fi

#############
# ######### #
# # Other # #
# ###########
#############

# Turn off X bell, thanks to the xset user preference utility for X
# - if xset available;
# - and if there is a display.
if command -v xset &> /dev/null && [ -n "$DISPLAY" ]; then
    xset b off
fi

# Start gpg-agent
# 'gpg-agent' is started on demand by GnuPG commands but not by
# OpenSSH commands, so force it to start.
# Check if gpg-agent is installed and available
if command -v gpg-agent > /dev/null; then
    # Start gpg-agent with SSH support if it's not already running
    if ! pgrep -x "gpg-agent" > /dev/null; then
        eval $(gpg-agent --daemon --enable-ssh-support)
    fi
fi

#################
# ############# #
# Honor .bashrc #
# ############# #
#################

# Honor per-interactive-shell startup file
if [ -f ~/.bashrc ]; then . ~/.bashrc; fi
