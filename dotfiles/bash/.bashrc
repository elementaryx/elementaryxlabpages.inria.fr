# -*- mode: sh -*-

#########################################################
# Example of a .bashrc coupled with .bashrc.elementaryx #
#########################################################

###############################
# ########################### #
# Begin typical .bashrc setup #
# ########################### #
###############################

# Export 'SHELL' to child processes.  Programs such as 'screen'
# honor it and otherwise use /bin/sh.
export SHELL

# if [[ $- != *i* ]]
# then
#     # We are being invoked from a non-interactive shell.  If this
#     # is an SSH session (as in "ssh host command"), source
#     # /etc/profile so we get PATH and other essential variables.
#     [[ -n "$SSH_CLIENT" ]] && source /etc/profile

#     # Don't do anything else.
#     return
# fi

# If the shell is not running interactively, exit this script
# Check the shell flags in $- to see if 'i' (interactive) is present
case $- in
    *i*)
        # If 'i' is present, the shell is interactive.
        # Continue executing the rest of .bashrc.
        ;;
    *)
        # If 'i' is not present, the shell is non-interactive.
        # Exit this script to prevent non-interactive shells
        # from running the rest of .bashrc.
        return
        ;;
esac

# # Source the system-wide file.
# source /etc/bashrc

# Adjust the prompt (PS1) depending on whether we're in 'guix environment'.
# It will be overriden by ~/.bashrc.elementaryx below, if the latter exists
if [ -n "$GUIX_ENVIRONMENT" ]
then
    PS1='\u@\h \w [env]\$ '
else
    PS1='\u@\h \w\$ '
fi

# Define common aliases
alias ls='ls -p --color=auto'
alias ll='ls -l'
alias grep='grep --color=auto'

###############################
# ########################### #
# End   typical .bashrc setup #
# ########################### #
###############################

###############################
# ########################### #
# Begin refined .bashrc setup #
# ########################### #
###############################

################
# bash history #
################

# Enable history appending
shopt -s histappend

# Set large history size
export HISTSIZE=1000000        # Number of commands to keep in the current session
export HISTFILESIZE=1000000    # Number of commands to keep in the history file

# Write history to file continuously
export PROMPT_COMMAND='history -a'

# Avoid duplicate entries
export HISTCONTROL=ignoredups:erasedups

#####################################
# Support dynamic terminal resizing #
#####################################

# Automatically check and update terminal size after each command
shopt -s checkwinsize

##########
# EDITOR #
##########

# EDITOR will be overriden in ~/.bashrc.elementaryx below, if the latter exists
export EDITOR='emacs -q --no-splash'
# export VISUAL="emacs"
export ALTERNATE_EDITOR="vim"

###############
# Elementaryx #
###############

# Load "elementaryx .bashrc", if available
if [ -f ~/.bashrc.elementaryx ]; then . ~/.bashrc.elementaryx; fi

###############################
# ########################### #
# End   refined .bashrc setup #
# ########################### #
###############################

